package com.netfin.thuchanh_caculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.text.DecimalFormat

class MainActivity : AppCompatActivity(), View.OnClickListener {

    var arrayPhantuA = mutableListOf<Double>()
    var arrayToantuA = mutableListOf<String>()
    var nhapSo:Boolean = true
    var start = true
    var epkieuText=""
    val fm =DecimalFormat("###.###")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn_so0.setOnClickListener (this)
        btn_so1.setOnClickListener (this)
        btn_so2.setOnClickListener (this)
        btn_so3.setOnClickListener (this)
        btn_so4.setOnClickListener (this)
        btn_so5.setOnClickListener (this)
        btn_so6.setOnClickListener (this)
        btn_so7.setOnClickListener (this)
        btn_so8.setOnClickListener (this)
        btn_so9.setOnClickListener (this)
        btn_daucham.setOnClickListener (this)
        btn_cong.setOnClickListener (this)
        btn_tru.setOnClickListener (this)
        btn_nhan.setOnClickListener (this)
        btn_chia.setOnClickListener (this)
        btn_bang.setOnClickListener (this)
        btn_xoakytu.setOnClickListener (this)
        btn_del.setOnClickListener (this)
        btn_soam.setOnClickListener (this)

        //tinhtoan(arrayListA,arrayToantuA)
    }
    override fun onClick(p0: View?) {
        if (p0 is Button){
            when (p0.text) {
                "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" -> {
                    if (start) {
                        arrayPhantuA.clear()
                        arrayToantuA.clear()
                        start=false
                        tv_nhapdulieu.text =""
                        tv_pheptoan.text=""
                    }
                        tv_nhapdulieu.append(p0.text)
                        epkieuText = tv_nhapdulieu.text.toString()
                        tv_nhapdulieu.text= fm.format(epkieuText.toDouble()).toString()
                        showPheptoan(arrayPhantuA, arrayToantuA)
                        nhapSo = true

                }
                "." -> {
                    if (start) {
                        arrayPhantuA.clear()
                        arrayToantuA.clear()
                        start=false
                        tv_nhapdulieu.text =""
                        tv_pheptoan.text=""
                    }
                    tv_nhapdulieu.append(p0.text)
                }
                "+/-" -> {
                    if (start) {
                        arrayPhantuA.clear()
                        arrayToantuA.clear()
                        start=false
                        tv_nhapdulieu.text =""
                        tv_pheptoan.text=""
                    }
                    if (TextUtils.isEmpty(tv_nhapdulieu.text)) {
                        tv_nhapdulieu.text ="0"
                    }
                    epkieuText = tv_nhapdulieu.text.toString()
                    tv_nhapdulieu.text= fm.format(-1*epkieuText.toDouble()).toString()
                    showPheptoan(arrayPhantuA, arrayToantuA)
                    nhapSo = true
                }
                "C" -> {
                    tv_nhapdulieu.text=""
                }
                "DEL" -> {
                    arrayPhantuA.clear()
                    arrayToantuA.clear()
                    start=true
                    nhapSo = true
                    tv_nhapdulieu.text =""
                    tv_pheptoan.text=""
                }
                "+","-","x",":" -> {
                    if(!start) {
                        if (nhapSo) {
                            arrayPhantuA.add(tv_nhapdulieu.text.toString().toDouble())
                            arrayToantuA.add(p0.text.toString())
                            tv_nhapdulieu.text = ""
                            nhapSo = false
                            showPheptoan(arrayPhantuA, arrayToantuA)
                        } else {
                            arrayToantuA.set(arrayPhantuA.lastIndex, p0.text.toString())
                            showPheptoan(arrayPhantuA, arrayToantuA)
                        }
                    } else Toast.makeText(this,"Please enter the elements!",Toast.LENGTH_SHORT).show()
                }
                "=" -> {
                    if (!start) {
                        start=true
                        if (nhapSo) {
                            arrayPhantuA.add(tv_nhapdulieu.text.toString().toDouble())
                            showPheptoan(arrayPhantuA, arrayToantuA)
                            tv_nhapdulieu.text = "= " + fm.format(tinhToan(arrayPhantuA, arrayToantuA))
                        } else {
                            showPheptoan(arrayPhantuA, arrayToantuA)
                            arrayToantuA.removeAt(arrayToantuA.lastIndex)
                            tv_nhapdulieu.text = "= " + fm.format(tinhToan(arrayPhantuA, arrayToantuA))
                        }
                    } else Toast.makeText(this,"Please enter the elements!",Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
    fun tinhToan(arrayList:MutableList<Double>,arrayToantu: MutableList<String>): Double{

        var soPheptoan = arrayToantu.size-1
        var index = 0
        while (index<= soPheptoan){
            when(arrayToantu[index]){
                "x" ->{
                    arrayList[index]*=arrayList[index+1]
                    arrayList.removeAt(index+1)
                    arrayToantu.removeAt(index)
                    index=0
                    soPheptoan = arrayToantu.size-1
                    }
                ":" ->{
                    arrayList[index]/=arrayList[index+1]
                    arrayList.removeAt(index+1)
                    arrayToantu.removeAt(index)
                    index=0
                    soPheptoan = arrayToantu.size-1
                    }
            }
            index++
        }
        var ketqua = arrayList[0]
        for (a in 0..soPheptoan){
            when(arrayToantu[a]){
                "+" -> ketqua+=arrayList[a+1]
                "-" -> ketqua-=arrayList[a+1]
            }
        }
        return ketqua

    }

    fun showPheptoan(arrayPhantu:MutableList<Double>,arrayToantu: MutableList<String>){
        var soPhantu = arrayPhantu.size-1
        var soToantu = arrayToantu.size-1
        tv_pheptoan.text=""
        for (a in 0..soPhantu) {
            tv_pheptoan.append(fm.format(arrayPhantuA[a]).toString())
            if (arrayToantu.size != 0&&a<=soToantu) {
                tv_pheptoan.append(arrayToantu[a])
            }
        }
    }
}
